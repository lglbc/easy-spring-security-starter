package io.gitee.lglbc.easy.security.open;

import io.gitee.lglbc.easy.security.core.exception.TokenException;
import io.gitee.lglbc.easy.security.core.util.JsonResponseUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 乐哥聊编程
 */
@Component
public class DefaultSecurityResultHandler implements EasySecurityResultHandler {
    @Override
    public void noPermissionHandler(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) {
        Map<String, Object> result = new HashMap<>();
        result.put("code", -1);
        result.put("msg", "无权限访问 默认");
        result.put("data", accessDeniedException.getMessage());
        JsonResponseUtil.out(response, result);
    }

    @Override
    public void noPermissionHandler(HttpServletRequest request, HttpServletResponse response, AuthenticationException authenticationException) {
        Map<String, Object> result = new HashMap<>();
        result.put("code", -1);
        result.put("msg", "禁止访问 默认");
        result.put("data", authenticationException.getMessage());
        JsonResponseUtil.out(response, result);
    }


    @Override
    public void loginSuccessHandler(HttpServletRequest request, HttpServletResponse response, Authentication authentication, String token) {
        Map<String, Object> result = new HashMap<>();
        result.put("code", -1);
        result.put("msg", "登录成功 默认");
        result.put("data", token);
        JsonResponseUtil.out(response, result);
    }

    @Override
    public void loginFailedHandler(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
        Map<String, Object> result = new HashMap<>();
        result.put("code", -1);
        result.put("msg", "登录失败 默认");
        result.put("data", exception.getMessage());
        JsonResponseUtil.out(response, result);
    }

    @Override
    public void tokenVerifyFailed(HttpServletResponse response, TokenException tokenException) {
        Map<String, Object> result = new HashMap<>();
        result.put("code", tokenException.getCode());
        result.put("msg", tokenException.getMsg());
        result.put("data", "默认实现");
        JsonResponseUtil.out(response, result);
    }
}
