package io.gitee.lglbc.easy.security.core.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author 乐哥聊编程
 */
@ConfigurationProperties(prefix = "easy.token")
@Data
public class EasyTokenProperties {
    private int expireTime;
    private String jksSecret;
    private String jksName;
    private boolean enableRSA;
    private String secret;
}
