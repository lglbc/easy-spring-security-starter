package io.gitee.lglbc.easy.security.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.List;

/**
 * @author 乐哥聊编程
 */
@ConfigurationProperties(prefix = "easy.security")
@EnableConfigurationProperties
@Data
public class EasySecurityProperties {
    private String jksSecret;
    private String jksName;
    private boolean enableRSA;
    private boolean captchaEnable;
    private String secret;
    private List<String> ignoreUrls;

    public List<String> getIgnoreUrls() {
        return ignoreUrls;
    }

    public void setIgnoreUrls(List<String> ignoreUrls) {
        this.ignoreUrls = ignoreUrls;
    }

    public boolean isCaptchaEnable() {
        return captchaEnable;
    }

    public void setCaptchaEnable(boolean captchaEnable) {
        this.captchaEnable = captchaEnable;
    }
}
