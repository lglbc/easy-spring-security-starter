package io.gitee.lglbc.easy.security.core.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * @author 乐哥聊编程
 */
public class JsonResponseUtil {
    public static void out(HttpServletResponse response, Object content) {
        response.setContentType("application/json;charset=utf-8");
        try {
            response.getWriter().println(new ObjectMapper().writeValueAsString(content));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
