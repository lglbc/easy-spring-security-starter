package io.gitee.lglbc.easy.security.core.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author 乐哥聊编程
 */
@ComponentScan("io.gitee.lglbc.easy.security")
@Slf4j
public class EasySecurityAutoConfiguration {
    public EasySecurityAutoConfiguration(){
        log.info("easy security start load ...");
    }
}
