package io.gitee.lglbc.easy.security.test;

import io.gitee.lglbc.easy.security.open.SimpleUser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 乐哥聊编程
 */
@Configuration
public class TestAuto {
    @Bean
    public SimpleUser newUser() {
        System.out.println("测试auto");
        return new SimpleUser();
    }
}
