package io.gitee.lglbc.easy.security.test;

import io.gitee.lglbc.easy.security.open.EasyLogin;
import io.gitee.lglbc.easy.security.open.SimpleUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 乐哥聊编程
 */
@Component
@Slf4j
public class TestEasyLogin implements EasyLogin {
    @Override
    public SimpleUser loadUser(String userName) {
        SimpleUser simpleUser = new SimpleUser();
        simpleUser.setUserName(userName);
        simpleUser.setPassword("123456");
        simpleUser.setPermissions(List.of("A","B"));
        simpleUser.setRoles(List.of("user","admin"));
        return simpleUser;
    }
}
