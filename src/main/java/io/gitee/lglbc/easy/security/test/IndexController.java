package io.gitee.lglbc.easy.security.test;

import io.gitee.lglbc.easy.security.core.util.EasySecurityUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 乐哥聊编程
 */
@RestController
public class IndexController {
    @GetMapping("/test")
    @PreAuthorize("hasRole('user')")
    public String test(Authentication authentication) {
        return EasySecurityUtil.getLoginUserName();
    }
}
