package io.gitee.lglbc.easy.security.core.util;

import io.gitee.lglbc.easy.security.open.SimpleUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Objects;

/**
 * @author 乐哥聊编程
 */
public class EasySecurityUtil {

    public static String getLoginUserName() {
        SimpleUser simpleUser = getLoginUser();
        return simpleUser.getUserName();
    }

    public static <T> T getLoginUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (Objects.isNull(authentication)) {
            throw new RuntimeException("user not login");
        }
        return (T) authentication.getPrincipal();
    }
}
