package io.gitee.lglbc.easy.security.core.token;

import lombok.Data;

import java.util.List;
import java.util.Map;
/**
 * @author 乐哥聊编程
 */
@Data
public class EasyPayload {

    private String sub;

    private Long iat;

    private Long exp;

    private String jti;

    private String username;

    private List<String> authorities;
    private Map<String, String> extInfo;
}
