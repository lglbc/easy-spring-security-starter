package io.gitee.lglbc.easy.security.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.gitee.lglbc.easy.security.core.exception.TokenException;
import io.gitee.lglbc.easy.security.open.EasySecurityResultHandler;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.annotation.Primary;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 乐哥聊编程
 */
//@Component
@Primary
public class TestEasySecurityResultHandler implements EasySecurityResultHandler {
    @Override
    public void noPermissionHandler(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) {
        Map<String, Object> result = new HashMap<>();
        result.put("code", -1);
        result.put("msg", "无权限访问");
        result.put("data", accessDeniedException.getMessage());
        writeResp(result, response);
    }

    @Override
    public void noPermissionHandler(HttpServletRequest request, HttpServletResponse response, AuthenticationException authenticationException) {
        Map<String, Object> result = new HashMap<>();
        result.put("code", -1);
        result.put("msg", "禁止访问");
        result.put("data", authenticationException.getMessage());
        writeResp(result, response);
    }


    @Override
    public void loginSuccessHandler(HttpServletRequest request, HttpServletResponse response, Authentication authentication, String token) {
        Map<String, Object> result = new HashMap<>();
        result.put("code", -1);
        result.put("msg", "登录成功");
        result.put("data", token);
        writeResp(result, response);
    }

    @Override
    public void loginFailedHandler(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
        Map<String, Object> result = new HashMap<>();
        result.put("code", -1);
        result.put("msg", "登录失败");
        result.put("data", exception.getMessage());
        writeResp(result, response);
    }

    @Override
    public void tokenVerifyFailed(HttpServletResponse response, TokenException tokenException) {
        Map<String, Object> result = new HashMap<>();
        result.put("code", tokenException.getCode());
        result.put("msg", tokenException.getMsg());
        writeResp(result, response);
    }

    public void writeResp(Object content, HttpServletResponse response) {
        response.setContentType("application/json;charset=utf-8");
        try {
            response.getWriter().println(new ObjectMapper().writeValueAsString(content));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
