package io.gitee.lglbc.easy.security.open;

import java.util.Map;

/**
 * @author 乐哥聊编程
 */
public interface EasyLogin {
    SimpleUser loadUser(String userName);
    default void preCheck(SimpleUser simpleUser) {}
    default Object saveCaptcha(String text, String imgBase64){ return null;}
    default void checkCaptcha(Map<String, String> map){}
}
