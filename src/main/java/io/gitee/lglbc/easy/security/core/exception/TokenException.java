package io.gitee.lglbc.easy.security.core.exception;

/**
 * @author 乐哥聊编程
 */
public class TokenException extends RuntimeException {
    private String code;
    private String msg;

    public TokenException(String code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
