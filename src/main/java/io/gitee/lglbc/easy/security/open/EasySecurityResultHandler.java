package io.gitee.lglbc.easy.security.open;

import io.gitee.lglbc.easy.security.core.exception.TokenException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * @author 乐哥聊编程
 */
public interface EasySecurityResultHandler {
    void noPermissionHandler(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException);
    void noPermissionHandler(HttpServletRequest request, HttpServletResponse response, AuthenticationException authenticationException);
    void loginFailedHandler(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception);
    void loginSuccessHandler(HttpServletRequest request, HttpServletResponse response, Authentication authentication, String token);
    void tokenVerifyFailed(HttpServletResponse response, TokenException tokenException);
}
