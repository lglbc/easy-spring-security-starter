package io.gitee.lglbc.easy.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @author 乐哥聊编程
 */
@SpringBootApplication
public class EasySpringSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasySpringSecurityApplication.class, args);
    }

}
