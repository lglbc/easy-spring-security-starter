## 版本说明
基于Spring Security 6开发
## 快速集成教程
### 添加仓库
任选其中一个
```
    maven { url "https://s01.oss.sonatype.org/service/local/repositories/releases/content" }
    maven { url "https://repo1.maven.org/maven2" }

```

### 引入依赖
* gradle
```
dependencies {
    implementation 'io.gitee.lglbc:easy-spring-security-starter:1.0.2'
}
```
* maven
```
<dependency>
  <groupId>io.gitee.lglbc</groupId>
  <artifactId>easy-spring-security-starter</artifactId>
  <version>1.0.2</version>
</dependency>
```
### 定义用户信息获取方式
因为在用户登录校验时，需要根据账号去查询密码，权限，所以这部分需要自己定义，在这里我们只需要实现EasyLogin接口即可，实现自己的业务逻辑.你可以引入MyBatis查询，我这边演示是基于内存Map存储用户信息
```
@Component
public class TestEasyLogin implements EasyLogin {
    public static Map<String,String> userMap = Map.of(
            "lglbc","{noop}123456",
            "ams","{noop}45678"
    );
    @Override
    public SimpleUser loadUser(String userName) {
        if (!userMap.containsKey(userName)) {
            return null;
        }
        String password = userMap.get(userName);
        SimpleUser simpleUser = new SimpleUser();
        simpleUser.setUserName(userName);
        simpleUser.setPassword(password);
        simpleUser.setPermissions(List.of("A","B"));
        simpleUser.setRoles(List.of("user","admin"));
        return simpleUser;
    }
}
```
### 启动项目验证
#### 登录成功
```
curl --location 'http://localhost:8080/login' \
--header 'Content-Type: application/json' \
--header 'Cookie: JSESSIONID=BAC13762365A696CBC3A58C84775928D' \
--data '{
    "username":"lglbc",
    "password":"123456"
}'
```

```
{"msg":"登录成功 默认","code":-1,"data":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdXRob3JpdGllcyI6WyJBIiwiQiIsIlJPTEVfYWRtaW4iLCJST0xFX3VzZXIiXSwiZXhwIjoxNjg4MzA5NTQ5NzE2LCJpYXQiOjE2ODgzMDU5NDk3MTYsImp0aSI6ImUzOTZlYzQ3LTY0N2MtNGRlNS1hMTNhLWIyNTI5OWMyMWQ3ZiIsInN1YiI6ImxnbGJjIiwidXNlcm5hbWUiOiJsZ2xiYyJ9.gSgb0MyZUapwQ6qO2oPNrAzhN9e8ifojFnGjJJ6q7KybYapSjWf8j5CIbymX_YOM8acM5vgwsyKjg_r_t7gBZ_ViWjxVdGkFtuMz0440aXrOH9U8TZSk74dWyzr7unIw4e8J1bGUZyPei1CBfDihlhHKitpVqWsXzKd_WYUJ5COqDkwIYI_P6rKR_mKniMHVLFln0Fubx2Pry-66Tid98KYIWhJMR6OKGK4RFtMoX19EcEysXwM4KatwkCXhgXq8nVmdLCedX505bTe6mMgpmWr5zRMGrI0pG_na80OxirGbKe5W5rKz8m5SchtyuWd7zqQgb0IPpQCds5ucYHHNqg"}
```
#### 登录失败
```
curl --location 'http://localhost:8080/login' \
--header 'Content-Type: application/json' \
--header 'Cookie: JSESSIONID=BAC13762365A696CBC3A58C84775928D' \
--data '{
    "username":"lglbc2",
    "password":"123456"
}'
```
```
{"msg":"登录失败 默认","code":-1,"data":"用户名或密码错误"}
```
#### 调用没权限接口
```
{"msg":"无权限访问 默认","code":-1,"data":"Access Denied"}
```
#### token 验证失败或过期
```
{"msg":"token verify failed","code":"403","data":"默认实现"}
```
## 高级用法
### 自定义返回结果
每个项目的返回结果的结构肯定都不一样，默认的返回结果肯定不满足需求，所以我们可以自定义返回结果，只需要实现接口EasySecurityResultHandler里面的方法即可.需要注意的是我们需要加上@Primary注解才能覆盖默认实现。
```
@Component
@Primary
public class CustomExceptionResult implements EasySecurityResultHandler {
    @Override
    public void noPermissionHandler(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) {
        JsonResponseUtil.out(response, "无权限访问 自定义");
    }

    @Override
    public void noPermissionHandler(HttpServletRequest request, HttpServletResponse response, AuthenticationException authenticationException) {
        JsonResponseUtil.out(response, "认证失败 自定义");
    }

    @Override
    public void loginFailedHandler(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
        JsonResponseUtil.out(response, "登录失败 自定义");
    }

    @Override
    public void loginSuccessHandler(HttpServletRequest request, HttpServletResponse response, Authentication authentication, String token) {
        JsonResponseUtil.out(response, token+" 自定义");
    }

    @Override
    public void tokenVerifyFailed(HttpServletResponse response, TokenException tokenException) {
        JsonResponseUtil.out(response, "token验证失败 自定义" + tokenException.getMsg());
    }
}
```
### 忽略不需要验证的接口
只需要在配置文件中配置list即可
```
easy:
  security:
    ignoreUrls:
      - /test10
      - /test20
```
### 自定义token密钥
因为系统默认使用对称加密密钥为123456，项目都需要使用自己的密钥
```
easy:
  security:
    ignoreUrls:
      - /test10
  token:
    expireTime: 3600
    secret: 123456
```

### 使用非对称密钥加密token
如果开启了enableRSA。需要填写jksSecret和jksPath
```
easy:
  token:
    expireTime: 3600
    enableRSA: true
    jksSecret: 123456
    jksName: /Users/lglbc/Desktop/code/easy-spring-security-starter-demo/src/main/resources/jwt.jks
```

### 添加验证码验证
实现EasyLogin->preCheck方法，系统在进行用户名密码认证之前会调用这个方法，所以基于这个方法我们可以去自定义验证码登录逻辑，系统也提供了生成验证码的接口 /kaptcha,并且会回调EasyLogin->saveCaptcha 触发验证码保存映射关系逻辑，这一部分需要自己实现

### 更多功能
更多功能正在开发中.希望大家能够提出更多宝贵的建议和需求.
### 最后
如果想参与进来一起维护这个开源项目，可以加我的微信“AmsNeil”，备注 ‘开源’